; Copyright 2001 Pace Micro Technology plc
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
; >s.Module

;DebugToolsSWI_Base	*	0x000554C0
DebugToolsErr		*	0x00819C00
TAB			*	9
LF			*	10
CR			*	13

; Patching of the kernel's IRQ handler to collect timing statistics needs
; updating to cope with HAL + 32bit world
        GBLL IRQPatch
IRQPatch SETL {FALSE}

; Unknown IRQ tracking is also broken (although to a lesser extent). Leave it
; disabled until we have a reason to fix it.
        GBLL UnkIRQs
UnkIRQs SETL {FALSE}

; ****************************************************
; *
; * The DebugTools module workspace format
; *
			^	0
WSP_PageSize		#	4		; The page size of the machine (-1)
WSP_VecPtrTab		#	4		; The base of the vector pointer table
WSP_TickNodeChain	#	4		; The pointer to the start of the ticker node chain
WSP_AppSlotBase		#	4		; The base address of the application space
WSP_AppSlotTop		#	4		; The end address of the application space
WSP_RMABase             #       4               ; The base address of the RMA space
WSP_RMATop              #       4               ; The end address of the  RMA space
WSP_ROMBase		#	4		; The base address of the ROM
WSP_UtilMod		#	4		; The base address of the Utility Module
WSP_ROMTop		#	4		; The end address of the ROM
 [ IRQPatch
WSP_IRQactive		#	4		; Flags for IRQ patching (0 - inactive, 1 - active)
 ]
WSP_IRQtable		#	4		; The address of the word after the IRQ despatch instruction
WSP_IRQtableEnd		#	4		; The address of the word after the IRQ despatch table
WSP_KernelSWIs		#	4		; The base of the Kernel's SWI despatch table
WSP_SWIDespatch		#	4		; Pointer to our SWI despatch code in RMA
WSP_SWIPatch		#	4		; Pointer to our SWI patch table in RMA
 [ IRQPatch
WSP_IRQcode		#	4		; The address of our IRQ patch code in RMA
 ]
WSP_DeadMods		#	4		; A count of the number of *RMRemoved modules
WSP_DirStack		#	4		; Pointer to the top of the directory stack (may be null)
WSP_IsKernel5xx		#	1		; Non zero if 5.xx
			#	3
WSP_ScratchBuffer	#	48		; A scratch buffer in our workspace
 [ IRQPatch
WSP_IRQinfo		#	(IRQs+1)*8	; A block of information for IRQ devices
 ]
 [ UnkIRQs
WSP_UnkIRQCount		#	IRQs*4		; A block of counters for the number of unclaimed IRQs
 ]
WSP_SIZE		*	@		; The size in bytes of our workspace


  [ DEBUG
  ! 0,"WSP_PageSize      &" :CC: :STR: WSP_PageSize
  ! 0,"WSP_VecPtrTab     &" :CC: :STR: WSP_VecPtrTab
  ! 0,"WSP_TickNodeChain &" :CC: :STR: WSP_TickNodeChain
  ! 0,"WSP_AppSlotBase   &" :CC: :STR: WSP_AppSlotBase
  ! 0,"WSP_AppSlotTop    &" :CC: :STR: WSP_AppSlotTop
  ! 0,"WSP_ROMBase       &" :CC: :STR: WSP_ROMBase
  ! 0,"WSP_UtilMod       &" :CC: :STR: WSP_UtilMod
  ! 0,"WSP_ROMTop        &" :CC: :STR: WSP_ROMTop
  ! 0,"WSP_IRQactive     &" :CC: :STR: WSP_IRQactive
  ! 0,"WSP_IRQtable      &" :CC: :STR: WSP_IRQtable
  ! 0,"WSP_KernelSWIs    &" :CC: :STR: WSP_KernelSWIs
  ! 0,"WSP_SWIDespatch   &" :CC: :STR: WSP_SWIDespatch
  ! 0,"WSP_SWIPatch      &" :CC: :STR: WSP_SWIPatch
  ! 0,"WSP_IRQcode       &" :CC: :STR: WSP_IRQcode
  ! 0,"WSP_DeadMods      &" :CC: :STR: WSP_DeadMods
  ! 0,"WSP_DirStack      &" :CC: :STR: WSP_DirStack
  ! 0,"WSP_ScratchBuffer &" :CC: :STR: WSP_ScratchBuffer
  ! 0,"WSP_IRQinfo       &" :CC: :STR: WSP_IRQinfo
  ! 0,"WSP_UnkIRQCount   &" :CC: :STR: WSP_UnkIRQCount
  ! 0,"WSP_SIZE          &" :CC: :STR: WSP_SIZE
  ! 0,""
  ];DEBUG


	AREA	|Asm$$Code|, CODE, READONLY


; ****************************************************
; *
; * Module header block
; *
Module_BaseAddr
	&	Mod_Start-Module_BaseAddr	; Module start entry point offset
	&	Mod_Init-Module_BaseAddr	; Module intialisation entry point offset
	&	Mod_Die-Module_BaseAddr		; Module finalisation entry point offset
	&	Mod_Service-Module_BaseAddr	; Module service call entry point offset
	&	Mod_Title-Module_BaseAddr	; Module title string offset
	&	Mod_HelpStr-Module_BaseAddr	; Module version info string offset
	&	Mod_HC_Table-Module_BaseAddr	; Help and command keyword table offset
	&	DebugToolsSWI_Base		; SWI chunk
	&	Mod_SWIHandler-Module_BaseAddr	; Module SWI handler entry point offset
	&	Mod_SWITable-Module_BaseAddr	; Module SWI table offset
	&	0				; No SWI name decode entry
	&	0				; No messages file
	&	Mod_Flags-Module_BaseAddr	; Module flags word offset


; ****************************************************
; *
; * Module version, date (and copyright) string
; *
Mod_HelpStr
	DCB	"Debug Tools", 9
	DCB	"$Module_MajorVersion ($Module_Date)"
 [ Module_MinorVersion <> ""
	DCB	" $Module_MinorVersion"
 ]
	DCB	0
	ALIGN


; ****************************************************
; *
; * Module flags word
; *
Mod_Flags
 [ No32bitCode
	&	0
 |
	&	ModuleFlag_32bit
 ]


; ****************************************************
; *
; * Module help and command keyword table
; *
Mod_HC_Table
	Command X, 255, 1		; *Command only - no SWI
	Command Where, 1, 0
	Command Vectors, 1, 0
	Command Tickers, 0, 0
	Command IRQDevices, 0, 0
 [ IRQPatch
	Command IRQInfo, 1, 0
 ]
	Command Canonical, 1, 1		; *Command only - no SWI
 [ UnkIRQs
	Command UnknownIRQs, 0, 0
 ]
	Command RMDie, 1, 1		; *Command only - no SWI
	Command RMRemove, 1, 1		; *Command only - no SWI
	Command PushD, 1, 0		; *Command only - no SWI
	Command PopD, 0, 0		; *Command only - no SWI
	Command PWD, 0, 0		; *Command only - no SWI
	Command EvalHex, 255, 1		; *Command only - no SWI
	Command EvalBin, 255, 1		; *Command only - no SWI
;	Command Poke, 4, 1		; *Command only - no SWI
;	Command Peek, 3, 1		; *Command only - no SWI
	Command	RMSave, 2, 2		; *Command only - no SWI
	Command	SaveWorkspace, 2, 2	; *Command only - no SWI
	Command	SaveDA, 2, 2		; *Command only - no SWI
	Command	RemoveDA, 1, 1		; *Command only - no SWI
	DCB	0

X_Help
	DCB	"*X will execute the specified CLI command and trap any errors in the "
	DCB	"system variable X$$Error (if not already set).", CR
X_Syntax
	DCB	"Syntax: *X <CLI command>", 0

Where_Help
	DCB	"*Where will output information on the location of the specified logical "
	DCB	"address (or that of the last exception).", CR
Where_Syntax
	DCB	"Syntax: *Where [<address>|<register>]", 0

Vectors_Help
	DCB	"*Vectors lists all of the software installed on the all or the "
	DCB	"specified software vectors.", CR
Vectors_Syntax
	DCB	"Syntax: *Vectors [<number> | <name>]", 0

Tickers_Help
	DCB	"*Tickers lists all CallAfters and CallEverys currently pending.", CR
Tickers_Syntax
	DCB	"Syntax: *Tickers", 0

IRQDevices_Help
	DCB	"*IRQDevices lists the current device drivers installed on each of the "
	DCB	"Kernel device IRQs vectors.", CR
IRQDevices_Syntax
	DCB	"Syntax: *IRQDevices", 0

 [ IRQPatch
IRQInfo_Help
	DCB	"*IRQInfo outputs information on the number of calls to and the duration "
	DCB	"of the handler for each IRQ.", CR
IRQInfo_Syntax
	DCB	"Syntax: *IRQInfo", 0
 ]

Canonical_Help
	DCB	"*Canonical will canonicalise the specified file name and place the "
	DCB	"result into the specified system variable.", CR
Canonical_Syntax
	DCB	"Syntax: *Canonical <sys var>", 0

 [ UnkIRQs
UnknownIRQs_Help
	DCB	"*UnknownIRQs outputs a count of the number of calls to IrqV for each "
	DCB	"device (since the last call to this *Command).", CR
UnknownIRQs_Syntax
	DCB	"Syntax: *UnknownIRQs", 0
 ]

RMDie_Help
	DCB	"*RMDie will remove a module's finalisation handler before killing the "
	DCB	"module.", CR
RMDie_Syntax
	DCB	"Syntax: *RMDie <module title>", 0

RMRemove_Help
	DCB	"*RMRemove will remove various handlers from a module but leave it in the "
	DCB	"module chain.", CR
RMRemove_Syntax
	DCB	"Syntax: *RMRemove <module title>", 0

PushD_Help
	DCB	"*PushD will push the currently selected directory (CSD) onto a stack of "
	DCB	"previous CSDs and move into the specified directory (if any).", CR
PushD_Syntax
	DCB	"Syntax: *PushD [<directory>]", 0

PopD_Help
	DCB	"*PopD pulls the most recently stacked directory off the directory stack and "
	DCB	"sets it as the currently selected directory (CSD).", CR
PopD_Syntax
	DCB	"Syntax: *PopD", 0

PWD_Help
	DCB	"*PWD outputs the name of the currently selected directory (CSD).", CR
PWD_Syntax
	DCB	"Syntax: *PWD", 0

EvalHex_Help
	DCB	"*EvalHex will evaluate the specified expression (as *Eval) and will return the "
	DCB	"result in eight-digit hexadecimal (if the result is not a string).", CR
EvalHex_Syntax
	DCB	"Syntax: *EvalHex <expression>", 0

EvalBin_Help
	DCB	"*EvalBin will evaluate the specified expression (as *Eval) and will return the "
	DCB	"result in 32 bit binary (if the result is not a string).", CR
EvalBin_Syntax
	DCB	"Syntax: *EvalBin <expression>", 0

;Poke_Help
;	DCB	"*Poke will store the specified value at the specified logical (or physical) "
;	DCB	"address.", CR
;Poke_Syntax
;	DCB	"Syntax *Poke [-P] [-U] [-B | -H | -D] <address> <value>", CR
;	DCB	TAB, "  -P  <address> is a physical address, else logical", CR
;	DCB	TAB, "  -U  perform the store with Usr mode permissions, else Svc mode", CR
;	DCB	TAB, "  -B  <value>   is a byte to store", CR
;	DCB	TAB, "  -H  <value>   is a halfword to store", CR
;	DCB	TAB, "  -D  <value>   is a doubleword to store", 0
;
;Peek_Help
;	DCB	"*Peek will output the contents of the specified logical (or physical) address.", CR
;Peek_Syntax
;	DCB	"Syntax *Peek [-P] [-U] [-B | -H | -D] <address>", CR
;	DCB	TAB, "  -P  <address> is a physical address, else logical", CR
;	DCB	TAB, "  -U  perform the store with Usr mode permissions, else Svc mode", CR
;	DCB	TAB, "  -B  value read is a byte", CR
;	DCB	TAB, "  -H  value read is a halfword", CR
;	DCB	TAB, "  -D  value read is a doubleword", 0

RMSave_Help
	DCB	"*RMSave will attempt to extract the specified module from memory and save it "
	DCB	"to the specified file.", CR
RMSave_Syntax
	DCB	"Syntax: *RMSave <module title> <filename>", 0

SaveWorkspace_Help
	DCB	"*SaveWorkspace will attempt to save a module's workspace to the specified file.", CR
SaveWorkspace_Syntax
	DCB	"Syntax: *SaveWorkspace <module title> <filename>", 0

SaveDA_Help
	DCB	"*SaveDA will save the first dynamic area found with a name "
	DCB	"matching that specified to the specified file.", CR
SaveDA_Syntax
	DCB	"Syntax: *SaveDA <area name> <filename>", 0

RemoveDA_Help
	DCB	"*RemoveDA will remove the first dynamic area found with a name "
	DCB	"matching that specified.", CR
RemoveDA_Syntax
	DCB	"Syntax: *RemoveDA <area name>", 0


; ****************************************************
; *
; * Module title string and module SWI table
; *
Mod_Title
Mod_SWITable
	DCB	"DebugTools", 0
	DCB	"AddressInfo", 0	; 0 SWI only - no *Command
	DCB	"Where", 0		; 1
	DCB	"Vectors", 0		; 2
	DCB	"Tickers", 0		; 3
	DCB	"IRQDevices", 0		; 4
	DCB	"IRQInfo", 0		; 5
	DCB	"UnknownIRQs", 0	; 6
	DCB	"ClaimOSSWI", 0		; 7
	DCB	"AddToOSSWI", 0		; 8
	DCB	"ReleaseOSSWI", 0	; 9
	DCB	0
	ALIGN


; ****************************************************
; *
; * Module execution start entry point
; *
Mod_Start * Module_BaseAddr


; ****************************************************
; *
; * Module initialisation entry point
; *
; * In...
; *   R10	= pointer to environment string
; *   R11	= I/O base or instantiation number
; *   R12	= private word pointer
; *   R13	= full-descending stack pointer
; *   R14	= return address
; * Out...
; *   R0-R6	= can be corrupted
; *   R7-R11	= must be preserved
; *   R12	= can be corrupted
; *   R13	= must be preserved
; *   R14	= can be corrupted
; *   V flag	= set if R0 is a pointer to an error block
; *
Mod_Init Entry "R10-R11"
	WriteLn	"InitTools..."
	MOV	R0, #ModHandReason_Claim
	MOV	R3, #WSP_SIZE
	SWI	XOS_Module
	BVS	%FT00
	STR	R2, [R12]
	MOV	R12, R2
	WriteLn	"  Wsp;"
	; Find out what the page size of this machine is...
	SWI	XOS_ReadMemMapInfo
	MOVVS	R0, #32*1024		; Assume 32KB page size if none returned
	SUB	R0, R0, #1
	STR	R0, [R12, #WSP_PageSize]
	;
	; Store known locations of certain things in memory, for which no interface
	; currently exists to find this information out...
	;
	MOV	R0, #6
	MOV	R1, #0
	MOV	R2, #OSRSI6_VecPtrTab   ; Base of vector pointer table
	SWI	XOS_ReadSysInfo
	MOVVS	R2, #0
	TEQ	R2, #0
	LDREQ	R2, =0x000007D8		; Legacy location
	STR	R2, [R12, #WSP_VecPtrTab]
        MOV     R0, #1                  ; RMA
        SWI     XOS_ReadDynamicArea
        MOVVS   R0, #0x20000000         ; Base of RMA space
        MOVVS   R2, #0x01C00000         ; Top of RMA space
        ADDVC   R2, R0, R2              ; R2 was the max size, we want the top
        STR     R0, [R12, #WSP_RMABase]
        STR     R2, [R12, #WSP_RMATop]
	MOV     R0, #-1
	SWI     XOS_ReadDynamicArea
	MOVVS   R0, #0x00008000		; Base of application space
	MOVVS   R2, #0x01C00000		; Top of application space
	ADDVC   R2, R0, R2              ; R2 was the max size, we want the top
	STR	R0, [R12, #WSP_AppSlotBase]
	STR	R2, [R12, #WSP_AppSlotTop]
	MOV	R0, #0
	STR	R0, [R12, #WSP_DeadMods]
	STR	R0, [R12, #WSP_DirStack]
	WriteLn	"  Tick;"
	; Find the address of the ticker node chain start pointer
	MOV	R0, #6
	MOV	R1, #0
	MOV	R2, #OSRSI6_TickNodeChain
	SWI	XOS_ReadSysInfo
	MOVVS	R2, #0
	STR	R2, [R12, #WSP_TickNodeChain]
	; Note kernel major version
	MOV	R0, #OsByte_OSVersionIdentifier
	MOV	R1, #0
	MOV	R2, #&FF
	SWI	XOS_Byte
	TEQ	R0, #&AA		; RISC OS 5.xx family
	MOVNE	R0, #0
	STRB	R0, [R12, #WSP_IsKernel5xx]
	; Find the base address of the Utility Module
	MOV	R0, #ModHandReason_LookupName
	ADR	R1, util_mod_title
	SWI	XOS_Module
	MOVVS	R3, #0
	STR	R3, [R12, #WSP_UtilMod]
	MOV     R0, R3, LSR #20
	MOV     R0, R0, LSL #20		; Base of the ROM = magabyte boundary below UtilityModule
	CMP     R0, #0x04000000
	MOVCC   R1, #0x04000000		; Top of the ROM
	MOVCS   R1, #0xFFFFFFFF
	STR	R0, [R12, #WSP_ROMBase]
	STR	R1, [R12, #WSP_ROMTop]
	WriteLn	"  IRQs;"
	BL	IRQ_init
	BVS	%FT00
	WriteLn	"  SWIs;"
	BL	SWI_patch
	WriteLn	"  Done."
00	;
	EXIT
util_mod_title
	DCB	"UtilityModule", 0
	ALIGN


; ****************************************************
; *
; * Module finalisation entry point
; *
; * In...
; *   R10	= fatality indication: 0 is non-fatal, 1 is fatal
; *   R11	= instantiation number
; *   R12	= private word pointer
; *   R13	= full-descending stack pointer
; *   R14	= return address
; * Out...
; *   R0-R6	= can be corrupted
; *   R7-R11	= must be preserved
; *   R12	= can be corrupted
; *   R13	= must be preserved
; *   R14	= can be corrupted
; *   V flag	= set if R0 is a pointer to an error block
; *
Mod_Die Entry "R10-R11"
	WriteLn	"DieTools..."
	LDR	R12, [R12]
	BL	IRQ_final
	WriteLn	"  SWIs;"
	BLVC	SWI_unpatch
	EXIT


; ****************************************************
; *
; * Dump a literal pool here
; *
	LTORG


; ****************************************************
; *
; * Module service call entry point
; *
; * In...
; *   R1	= service call reason code
; *   R12	= private word pointer
; *   R13	= full-descending stack pointer
; *   R14	= return address
; * Out...
; *   R0	= may be altered to return a result
; *   R1	= 0 if we claimed it
; *   R2-R8	= may be altered to return a result
; *   R9-R11	= must be preserved
; *   R12	= can be corrupted
; *   R13-R14	= must be preserved
; *
; * Do NOT return V set errors from this routine!
; *
Mod_Service * Module_BaseAddr
;Fast_SVC_Table
;	&	0				; Flags word
;	&	Mod_FastService-Module_BaseAddr	; Actual service call handler
;	&	Service_???
;	&	Service_????
;	&	0				; End of service call table
;	&	Fast_SVC_Table-Module_BaseAddr	; Offset of fast service table
;Mod_Service ROUT
;	MOV	R0, R0
;	TEQ	R1, #Service_???
;	TEQNE	R1, #Service_????
;	MOVEQ	PC, LR
;Mod_FastService Entry
;	EXIT


	END
